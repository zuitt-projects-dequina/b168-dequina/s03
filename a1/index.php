<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        body {
            border: 1px solid black;
            margin-left: 10%;
            margin-right: 10%;
            padding-left: 1rem;
            padding-right: 1rem;
        }
    </style>
    <title>S03 Activity</title>
</head>
<body>
    <h1>S03 Activity</h1>
    <hr/>

    <h2>Person</h2>
        <p><?php echo $Person->printName(); ?></p>
        
    <h2>Developer</h2>
        <p><?php echo $Developer->printName(); ?></p>
        
    <h2>Engineer</h2>
        <p><?php echo $Engineer->printName(); ?></p>
</body>
</html>