<?php

// Person Class
class Person {
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName, $middleName, $lastName){
        $this -> firstName = $firstName;
        $this -> middleName = $middleName;
        $this -> lastName = $lastName;
    }

    public function printName() {
        return "Your full name is $this->firstName $this->middleName $this->lastName.";
    }
}

$Person = new Person(
    'Ralph Louies',
    'Limbo',
    'Dequina'
);


// Developer Class
class Developer extends Person {
    public function printName() {
        return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer.";
    }
}

$Developer = new Developer(
    'Brandon',
    '',
    'Lee'
);

// Engineer Class
class Engineer extends Person {
    public function printName() {
        return "Your are an engineer named $this->firstName $this->middleName $this->lastName.";
    }
}

$Engineer = new Engineer(
    'Myers',
    '',
    'Reese'
);