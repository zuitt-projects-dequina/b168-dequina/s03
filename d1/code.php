<?php

/* The Object-Oriented Approach
    is a programming paradigm that uses objects and their interactions to design applications and computer programs.

The basic programming concepts in OOP are:
    - Abstraction
         - is simplifying complex reality by modeling classes appropriate to the problem
         - Only the onject's features are visible but the actual implementation are hidden
    - Polymorphism
        - is the process of using an operator or function in different ways for different data input
        - Methods inherited by a derived class can be overridden to have a behavior different from the method of base class.
    - Encapsulation
        - hides the implementation details of a class from other objects
        - Also know as "data binding", it dictates that data must not be directly accessible to users but through a public function called "property"
    - Inheritance
        - is a way to form new classes using classes that have already been defined
        - the derived classes are allowed to inherint variables and methods from a specified base class.
*/

/* 
    Classes - are blueprints that define values and behaviours
    Objects - are the implemtation of these classes
    Instances of a Class - are objects created from a class
*/

// Objects as Variables

    $buildingObj = (object) [
        'name' => 'Caswynn Building',
        'floors' => 8,
        'address' => (object) [
            'barangay' => 'Sacred Heart',
            'city' => 'Quezon City',
            'country' => 'Philippines' 
        ]
    ];

// Objects from Classes (Encapsulation)

    class Building {
        // public - the property or method can be accessed from everywhere
        public $name;
        public $floors;
        public $address;

        // Constructor - used during the creation of an object
            // A constructor allows you to initialize an object's properties upon creation of the object. If you create a __construct() function, PHP will automatically call this function when you create an object from a class
            public function __construct($name, $floors, $address){
                $this -> name = $name;
                $this -> floors = $floors;
                $this -> address = $address;
            }

            public function printName() {
                return "The name of the building is $this->name.";
            }
    }

    // Extends - used to derive a class from another class. This is called inheritance. A derived class has all of the public and protected properties of the class that it is derived from
    class Condominium extends Building{
        public $occupants;
        
        public function printName() {
            return "The name of the condominium is $this->name.";
        }
    }

    $building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

    $condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makari City, Philippines');

