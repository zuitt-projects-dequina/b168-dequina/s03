<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>S03: Classes, Objects, Inheritance and Polymorphism</title>
</head>
<body>
    <h1>Objects from Variables</h1>
        <p><?php echo $buildingObj->name; ?></p>
        <p><?php echo $buildingObj->address->country; ?></p>
    
    <h1>Objects And Classes</h1>
        <p><?php var_dump($building) ?></p>

        <p><?php echo $building->printName() ?></p>

        <h1>Inheritance (Condominium Object)</h1>
            <p><?php echo $condominium->name; ?></p>
            <p><?php echo $condominium->floors; ?></p>
            <p><?php echo $condominium->address; ?></p>

        <h1>Polymorpism (Changing of printName behaviour)</h1>
            <p><?php echo $condominium->printName() ?></p>

</body>
</html>